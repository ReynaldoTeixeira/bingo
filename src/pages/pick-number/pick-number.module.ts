import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickNumberPage } from './pick-number';

@NgModule({
  declarations: [
    PickNumberPage,
  ],
  imports: [
    IonicPageModule.forChild(PickNumberPage),
  ],
})
export class PickNumberPageModule {}
