import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PickNumberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pick-number',
  templateUrl: 'pick-number.html',
})
export class PickNumberPage {

  public randomNumber;
  public timeInterval;
  isActivated:boolean = false;
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PickNumberPage');
  }

  start(){
    this.timeInterval = setInterval(() => { this.randomNumber = Math.floor(Math.random() * 75) + 5; }, 100);
    this.isActivated = true;
  }

  stop(){
     clearInterval(this.timeInterval);
     this.isActivated = false;
  }

}
