import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PickNumberPage } from '../pick-number/pick-number';
import { NumberOfCardsPage } from '../number-of-cards/number-of-cards';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  
  constructor(public navCtrl: NavController) {

  }

  moveToPickNumber(){
    this.navCtrl.push(PickNumberPage);
  }

  moveToNumberOfCards(){
    this.navCtrl.push(NumberOfCardsPage);
  }
}
