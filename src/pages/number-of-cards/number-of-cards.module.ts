import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NumberOfCardsPage } from './number-of-cards';

@NgModule({
  declarations: [
    NumberOfCardsPage,
  ],
  imports: [
    IonicPageModule.forChild(NumberOfCardsPage),
  ],
})
export class NumberOfCardsPageModule {}
