import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CardsPage } from '../cards/cards';

/**
 * Generated class for the NumberOfCardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-number-of-cards',
  templateUrl: 'number-of-cards.html',
})
export class NumberOfCardsPage {

  public cardsQuantity;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NumberOfCardsPage');
  }

  moveToCards(){
    this.navCtrl.push(CardsPage,{
      quantity : this.cardsQuantity
    });
  }
}
