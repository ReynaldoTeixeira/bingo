import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the CardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html',
})
export class CardsPage {

  card = new Array(5);
  card2 = new Array(5);
  card3 = new Array(5);
  card4 = new Array(5);
  isActivated:boolean = true;
  public numberOfCards;
  public check = [];
  public check2 = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.numberOfCards = this.navParams.get("quantity");
    // console.log(this.numberOfCards);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad CardsPage');

    if(this.numberOfCards == 1){
      // Card 1
      for(let x=0;x<5;x++){
        this.card[x]=new Array(5);
        for(let y=0;y<5;y++){
          // Bingo Rules - Cards
          if(x==0 && y==0 || x==1 && y==0||x==2 && y==0||x==3 && y==0||x==4 && y==0){   
            this.card[x][y]= Math.floor(Math.random()*(16 - 1)+1); //1 - 15
          }else if(x==0 && y==1 || x==1 && y==1||x==2 && y==1||x==3 && y==1||x==4 && y==1){
            this.card[x][y]= Math.floor(Math.random()*(31 - 16) + 16);// 15 - 30
          }else if(x==0 && y==2 || x==1 && y==2||x==2 && y==2||x==3 && y==2||x==4 && y==2){
            this.card[x][y]= Math.floor(Math.random()*(46 - 31) + 31);// 31 - 45
          }else if (x==0 && y==3 || x==1 && y==3||x==2 && y==3||x==3 && y==3||x==4 && y==3){
            this.card[x][y]= Math.floor(Math.random()*(61 - 46) + 46);// 46 - 60
          }else if(x==0 && y==4 || x==1 && y==4||x==2 && y==4||x==3 && y==4||x==4 && y==4){
            this.card[x][y]= Math.floor(Math.random()*(76 - 61) + 61);// 61 - 75
          }

          //No-Repeat Numbers card 1
          if(this.check.indexOf(this.card[x][y])==-1){
            this.check.push(this.card[x][y]);
          }else{
            y--;
          }
          // end No-Repeat Numbers

          if(x == 2 && y == 2){
            this.card [x][y] = " ";
          }
        }
      }
      //end Card 1
    }else if(this.numberOfCards==2){
      // card 1
      for(let x=0;x<5;x++){
        this.card[x]=new Array(5);
        for(let y=0;y<5;y++){
          // Bingo Rules - Cards
          if(x==0 && y==0 || x==1 && y==0||x==2 && y==0||x==3 && y==0||x==4 && y==0){   
            this.card[x][y]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(x==0 && y==1 || x==1 && y==1||x==2 && y==1||x==3 && y==1||x==4 && y==1){
            this.card[x][y]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(x==0 && y==2 || x==1 && y==2||x==2 && y==2||x==3 && y==2||x==4 && y==2){
            this.card[x][y]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (x==0 && y==3 || x==1 && y==3||x==2 && y==3||x==3 && y==3||x==4 && y==3){
            this.card[x][y]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(x==0 && y==4 || x==1 && y==4||x==2 && y==4||x==3 && y==4||x==4 && y==4){
            this.card[x][y]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 1
          if(this.check.indexOf(this.card[x][y])==-1){
            this.check.push(this.card[x][y]);
          }else{
            y--;
          }
          // end No-Repeat Numbers
          if(x == 2 && y == 2){
            this.card [x][y] = " ";
          }
        }
      }
      // end card 1

      // card 2 
      for(let a=0;a<5;a++){
        this.card2[a]=new Array(5);
        for(let b=0;b<5;b++){
          // Bingo Rules - Cards
          if(a==0 && b==0 || a==1 && b==0||a==2 && b==0||a==3 && b==0||a==4 && b==0){   
            this.card2[a][b]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(a==0 && b==1 || a==1 && b==1||a==2 && b==1||a==3 && b==1||a==4 && b==1){
            this.card2[a][b]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(a==0 && b==2 || a==1 && b==2||a==2 && b==2||a==3 && b==2||a==4 && b==2){
            this.card2[a][b]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (a==0 && b==3 || a==1 && b==3||a==2 && b==3||a==3 && b==3||a==4 && b==3){
            this.card2[a][b]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(a==0 && b==4 || a==1 && b==4||a==2 && b==4||a==3 && b==4||a==4 && b==4){
            this.card2[a][b]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 2
          if(this.check2.indexOf(this.card2[a][b])==-1){
            this.check2.push(this.card2[a][b]);
          }else{
            b--;
          }
          // end No-Repeat Numbers

          if(a == 2 && b == 2){
            this.card2 [a][b] = " ";
          }
        }
      }
      // end card 2 
    }else if(this.numberOfCards==3){
      // card 1
      for(let x=0;x<5;x++){
        this.card[x]=new Array(5);
        for(let y=0;y<5;y++){
          // Bingo Rules - Cards
          if(x==0 && y==0 || x==1 && y==0||x==2 && y==0||x==3 && y==0||x==4 && y==0){   
            this.card[x][y]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(x==0 && y==1 || x==1 && y==1||x==2 && y==1||x==3 && y==1||x==4 && y==1){
            this.card[x][y]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(x==0 && y==2 || x==1 && y==2||x==2 && y==2||x==3 && y==2||x==4 && y==2){
            this.card[x][y]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (x==0 && y==3 || x==1 && y==3||x==2 && y==3||x==3 && y==3||x==4 && y==3){
            this.card[x][y]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(x==0 && y==4 || x==1 && y==4||x==2 && y==4||x==3 && y==4||x==4 && y==4){
            this.card[x][y]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 1
          if(this.check.indexOf(this.card[x][y])==-1){
            this.check.push(this.card[x][y]);
          }else{
            y--;
          }
          // end No-Repeat Numbers

          if(x == 2 && y == 2){
            this.card [x][y] = " ";
          }
        }
      }
      // end card 1

      // card 2
      for(let a=0;a<5;a++){
        this.card2[a]=new Array(5);
        for(let b=0;b<5;b++){
          // Bingo Rules - Cards
          if(a==0 && b==0 || a==1 && b==0||a==2 && b==0||a==3 && b==0||a==4 && b==0){   
            this.card2[a][b]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(a==0 && b==1 || a==1 && b==1||a==2 && b==1||a==3 && b==1||a==4 && b==1){
            this.card2[a][b]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(a==0 && b==2 || a==1 && b==2||a==2 && b==2||a==3 && b==2||a==4 && b==2){
            this.card2[a][b]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (a==0 && b==3 || a==1 && b==3||a==2 && b==3||a==3 && b==3||a==4 && b==3){
            this.card2[a][b]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(a==0 && b==4 || a==1 && b==4||a==2 && b==4||a==3 && b==4||a==4 && b==4){
            this.card2[a][b]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 2
          if(this.check2.indexOf(this.card2[a][b])==-1){
            this.check2.push(this.card2[a][b]);
          }else{
            b--;
          }
          // end No-Repeat Numbers

          if(a == 2 && b == 2){
            this.card2 [a][b] = " ";
          }
        }
      }
      // end card 2

      // card 3
      for(let c=0;c<5;c++){
        this.card3[c]=new Array(5);
        for(let d=0;d<5;d++){
          // Bingo Rules - Cards
          if(c==0 && d==0 || c==1 && d==0||c==2 && d==0||c==3 && d==0||c==4 && d==0){   
            this.card3[c][d]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(c==0 && d==1 || c==1 && d==1||c==2 && d==1||c==3 && d==1||c==4 && d==1){
            this.card3[c][d]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(c==0 && d==2 || c==1 && d==2||c==2 && d==2||c==3 && d==2||c==4 && d==2){
            this.card3[c][d]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (c==0 && d==3 || c==1 && d==3||c==2 && d==3||c==3 && d==3||c==4 && d==3){
            this.card3[c][d]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(c==0 && d==4 || c==1 && d==4||c==2 && d==4||c==3 && d==4||c==4 && d==4){
            this.card3[c][d]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 3
          if(this.check.indexOf(this.card3[c][d])==-1){
            this.check.push(this.card3[c][d]);
          }else{
            d--;
          }
          // end No-Repeat Numbers

          if(c == 2 && d == 2){
            this.card3 [c][d] = " ";
          }
        }
      }
      // end card 3
    }else if(this.numberOfCards==4){

      // card 1
      for(let x=0;x<5;x++){
        this.card[x]=new Array(5);
        for(let y=0;y<5;y++){
          // Bingo Rules - Cards
          if(x==0 && y==0 || x==1 && y==0||x==2 && y==0||x==3 && y==0||x==4 && y==0){   
            this.card[x][y]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(x==0 && y==1 || x==1 && y==1||x==2 && y==1||x==3 && y==1||x==4 && y==1){
            this.card[x][y]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(x==0 && y==2 || x==1 && y==2||x==2 && y==2||x==3 && y==2||x==4 && y==2){
            this.card[x][y]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (x==0 && y==3 || x==1 && y==3||x==2 && y==3||x==3 && y==3||x==4 && y==3){
            this.card[x][y]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(x==0 && y==4 || x==1 && y==4||x==2 && y==4||x==3 && y==4||x==4 && y==4){
            this.card[x][y]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 1
          if(this.check.indexOf(this.card[x][y])==-1){
            this.check.push(this.card[x][y]);
          }else{
            y--;
          }
          // end No-Repeat Numbers

          if(x == 2 && y == 2){
            this.card [x][y] = " ";
          }
        }
      }
      // end card 1

      // card 2 
      for(let a=0;a<5;a++){
        this.card2[a]=new Array(5);
        for(let b=0;b<5;b++){
          // Bingo Rules - Cards
          if(a==0 && b==0 || a==1 && b==0||a==2 && b==0||a==3 && b==0||a==4 && b==0){   
            this.card2[a][b]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(a==0 && b==1 || a==1 && b==1||a==2 && b==1||a==3 && b==1||a==4 && b==1){
            this.card2[a][b]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(a==0 && b==2 || a==1 && b==2||a==2 && b==2||a==3 && b==2||a==4 && b==2){
            this.card2[a][b]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (a==0 && b==3 || a==1 && b==3||a==2 && b==3||a==3 && b==3||a==4 && b==3){
            this.card2[a][b]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(a==0 && b==4 || a==1 && b==4||a==2 && b==4||a==3 && b==4||a==4 && b==4){
            this.card2[a][b]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

           //No-Repeat Numbers card 2
           if(this.check2.indexOf(this.card2[a][b])==-1){
            this.check2.push(this.card2[a][b]);
          }else{
            b--;
          }
          // end No-Repeat Numbers

          if(a == 2 && b == 2){
            this.card2 [a][b] = " ";
          }
        }
      }
      // end card 2

      // card 3
      for(let c=0;c<5;c++){
        this.card3[c]=new Array(5);
        for(let d=0;d<5;d++){
          // Bingo Rules - Cards
          if(c==0 && d==0 || c==1 && d==0||c==2 && d==0||c==3 && d==0||c==4 && d==0){   
            this.card3[c][d]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(c==0 && d==1 || c==1 && d==1||c==2 && d==1||c==3 && d==1||c==4 && d==1){
            this.card3[c][d]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(c==0 && d==2 || c==1 && d==2||c==2 && d==2||c==3 && d==2||c==4 && d==2){
            this.card3[c][d]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (c==0 && d==3 || c==1 && d==3||c==2 && d==3||c==3 && d==3||c==4 && d==3){
            this.card3[c][d]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(c==0 && d==4 || c==1 && d==4||c==2 && d==4||c==3 && d==4||c==4 && d==4){
            this.card3[c][d]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 3
          if(this.check.indexOf(this.card3[c][d])==-1){
            this.check.push(this.card3[c][d]);
          }else{
            d--;
          }
          // end No-Repeat Numbers

          if(c == 2 && d == 2){
            this.card3 [c][d] = " ";
          }
        }
      } 
      // end card 3

      // card 4
      for(let e=0;e<5;e++){
        this.card4[e]=new Array(5);
        for(let f=0;f<5;f++){
          // Bingo Rules - Cards
          if(e==0 && f==0 || e==1 && f==0||e==2 && f==0||e==3 && f==0||e==4 && f==0){   
            this.card4[e][f]= Math.floor(Math.random()*(16 - 1)+1);
          }else if(e==0 && f==1 || e==1 && f==1||e==2 && f==1||e==3 && f==1||e==4 && f==1){
            this.card4[e][f]= Math.floor(Math.random()*(31 - 16) + 16);
          }else if(e==0 && f==2 || e==1 && f==2||e==2 && f==2||e==3 && f==2||e==4 && f==2){
            this.card4[e][f]= Math.floor(Math.random()*(46 - 31) + 31);
          }else if (e==0 && f==3 || e==1 && f==3||e==2 && f==3||e==3 && f==3||e==4 && f==3){
            this.card4[e][f]= Math.floor(Math.random()*(61 - 46) + 46);
          }else if(e==0 && f==4 || e==1 && f==4||e==2 && f==4||e==3 && f==4||e==4 && f==4){
            this.card4[e][f]= Math.floor(Math.random()*(76 - 61) + 61);
          }
          // end Rules

          //No-Repeat Numbers card 4
          if(this.check2.indexOf(this.card4[e][f])==-1){
            this.check2.push(this.card4[e][f]);
          }else{
            f--;
          }
          // end No-Repeat Numbers

          if(e == 2 && f == 2){
            this.card4 [e][f] = " ";
          }
        }
      } 
    }
    //end card 4
// --------------------------end-----------------------------------    
  }




}
